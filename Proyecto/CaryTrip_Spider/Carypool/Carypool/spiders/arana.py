# -*- coding: utf-8 -*-
import scrapy
from Carypool.items import CarypoolItem

class AranaSpider(scrapy.Spider):
    name = 'arana'
    allowed_domains = ['liverpool.com.mx']
    start_urls = ['https://www.liverpool.com.mx/tienda?s=@holamundo']

    def parse(self, response):
        item = CarypoolItem()
        
        item['producto'] = response.xpath("//div[@class='o-listing__products']//li//div[@class='m-plp-card-container']//figure//figcaption//article//h5/text()").extract()
        item['precios'] = response.xpath("//div[@class='o-listing__products']//li//div[@class='m-plp-card-container']//figure//figcaption//p[@class='a-card-discount']/text()").extract()
        return item
