<?php
require_once('conexion.php');
$conn = conexion();
$tipoSelect = $_POST['tipoSelect'];;
if ($tipoSelect == 1) { ?>
    <div class="table-responsive">
        <table id="escalation" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">Nombre del Producto</th>
                <th class="text-center">Precio</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i=0;
            $query = mysqli_query($conn,"select *from spiderControl");
            while( $row = mysqli_fetch_array($query)) {
                $class = "";
                $classBtn = "success";
                $tittle = "RELACIONAR";
                $relacionar = "0";
                $url="https://www.liverpool.com.mx/tienda?s=".$row['nombre'];
                $i++; ?>
                <tr class="<?= $class?>">
                    <td class="text-center"><?= $i;?></td>
                    <td class="text-center">
                        <a href=<?= $url;?>>
                        <?= $row['nombre'];?>
                        </a>
                    </td>
                    <td class="text-center"><?= $row['precio'];?></td>
                </tr>
            <?php  }?>
            </tbody>
        </table>
    </div>
<?php }?>
<script>
    $(document).ready(function () {
        $('#escalation').DataTable( {
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
            }
        } );
        $('#escalation').DataTable();
    });
</script>
